# Hello! it's **Fuhrerh Lemon** <img src="https://stickerly.pstatic.net/sticker_pack/pco1gspFuK408WhSIdusEQ/1HX6ZX/19/21395b83-2fa8-4f19-aec9-98e8089dbb08.png" width="150px" border="0">

<h3 align="center">
  <a target="_blank" href="https://ask.fedoraproject.org/u/lemon/summary" target="_blank">
    <img src="https://img.shields.io/badge/fedora-06B6D4?style=for-the-badge&logo=linux&logoColor=white">
  </a>
  <a target="_blank" href="https://gitlab.com/Fuhrerh-Lemon" target="_blank">
    <img alt="Gitlab - FuhrerhLemon" src="https://img.shields.io/badge/gitlab-A9225C?&style=for-the-badge&logo=gitlab&logoColor=white">
  </a>
  <a target="_blank" href="https://www.hackerrank.com/FuhrerhLemon?hr_r=1" target="_blank">
    <img alt="HackerRank - FuhrerhLemon" src="https://img.shields.io/badge/hackerrank-00A98F?&style=for-the-badge&logo=hackerrank&logoColor=white">
  </a>
  <a target="_blank" href="https://www.kaggle.com/fuhrerhlemon" target="_blank">
    <img alt="Kaggle - FuhrerhLemon" src="https://img.shields.io/badge/kaggle-20BEFF?&style=for-the-badge&logo=kaggle&logoColor=white">
  </a>
  <a target="_blank" href="https://huggingface.co/Fuhrerh-Lemon" target="_blank">
    <img alt="Huggingface - FuhrerhLemon" src="https://img.shields.io/badge/hugginface-ECD53F?&style=for-the-badge&&logoColor=white">
  </a>
</h3>

```css
┌[FuhrerhLemon@GitHub]-(~)
└> neofetch 
```
<div style="display:block;text-align:left"><img align="left" src="https://stickerly.pstatic.net/sticker_pack/pco1gspFuK408WhSIdusEQ/1HX6ZX/19/68e74b79-61c7-4529-8351-f6f3b85f8741.png" border="0" style="width:180px;">
  
  ```css
  FuhrerhLemon@GitHub
  -----------------
  Name: FuhrerhLemon
  Lenguaje-Favorito: C++
  OS: Fedora
  Terminal: Kitty
  shell: Fish
  ```
</div><br>

```css
┌[FuhrerhLemon@GitHub]-(~)
└> sh ./About 
```
- 🌱 learning **Rust**.
- 👾 I use Linux **BTW**.
- 💻 My dotfiles -> ([Dtlemon](https://gitlab.com/Fuhrerh-Lemon/dtlemon)).

## 📊 **Weekly development breakdown**
<!--START_SECTION:waka-->
```text
Python       10 hrs 16 mins  ██████████████████▒░░░░░░   73.22 % 
HTML         1 hr 11 mins    ██░░░░░░░░░░░░░░░░░░░░░░░   08.55 % 
TOML         27 mins         ▓░░░░░░░░░░░░░░░░░░░░░░░░   03.32 % 
JSON         25 mins         ▓░░░░░░░░░░░░░░░░░░░░░░░░   03.07 % 
TypeScript   25 mins         ▓░░░░░░░░░░░░░░░░░░░░░░░░   02.99 % 
```
<!--END_SECTION:waka-->

## 🚀**My projects** 
<h4 align="left">

**Gitlab**
</h4>

- [ArchLemon: Instalador de archLinux](https://gitlab.com/Fuhrerh-Lemon/archlemon)
<h4 align="left">

**Github**
</h4>

- [Quantum: Practicas personales](https://github.com/Fuhrerh-Lemon/quantum-practices)
- [Knowledge-Graph](https://github.com/Fuhrerh-Lemon/knowledge-graph)
<h4 align="center">

**Programming languages**
</h4>
<h3 align="center">
  <img src="https://img.shields.io/badge/python%20-%2314354C.svg?style=for-the-badge&logo=python&logoColor=white">
  <img src="https://img.shields.io/badge/C++-000000?style=for-the-badge&logo=cplusplus&logoColor=white">
  <img src="https://img.shields.io/badge/haskell-5D4F85?style=for-the-badge&logo=haskell&logoColor=white">
</h3>
<h4 align="center">

**Editor**
</h4>
<h3 align="center">
  <a target="_blank" href="https://neovim.io/" target="_blank">
    <img src="https://img.shields.io/badge/neovim%20-%2357A143.svg?style=for-the-badge&logo=neovim&logoColor=white">
  </a>
  <a target="_blank" href="https://code.visualstudio.com/" target="_blank">
    <img src="https://img.shields.io/badge/vscode%20-%23007ACC.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white">
  </a>
  <a target="_blank" href="https://lapce.dev/" target="_blank">
    <img src="https://img.shields.io/badge/lapce-3B82F6.svg?style=for-the-badge&logo=lapce&logoColor=white">
  </a>
</h3>
<h4 align="center">

**Operating system [BASE]**
</h4>
<h3 align="center">
  <a target="_blank" href="https://getfedora.org/" target="_blank">
    <img src="https://img.shields.io/badge/fedora-06B6D4?style=for-the-badge&logo=linux&logoColor=white">
  </a>
</h3>
<h4 align="center">

**Operating system [KVM-Passthrough]**
</h4>
<h3 align="center">
  <img src="https://img.shields.io/badge/windows-0078D6?style=for-the-badge&logo=windows&logoColor=white">
</h3>
<h4 align="center">

**Package management**
</h4>
<h3 align="center">
  <a target="_blank" href="https://conan.io/" target="_blank">
    <img src="https://img.shields.io/badge/conan-6699CB?style=for-the-badge&logo=conan&logoColor=white">
  </a>
  <a target="_blank" href="https://python-poetry.org/" target="_blank">
    <img src="https://img.shields.io/badge/poetry-60A5FA?style=for-the-badge&logo=poetry&logoColor=white">
  </a>
</h3>
<h4 align="center">

**Dev Tools**
</h4>
<h3 align="center">
  <img src="https://img.shields.io/badge/git-F02E65?style=for-the-badge&logo=git&logoColor=FFFFFF">
  <img src="https://img.shields.io/badge/podman-9999FF?style=for-the-badge&logo=podman&logoColor=FFFFFF">
  <img src="https://img.shields.io/badge/qemu-EE6123?style=for-the-badge&logo=qemu&logoColor=FFFFFF">
  <img src="https://img.shields.io/badge/latex-008080?style=for-the-badge&logo=latex&logoColor=FFFFFF">
  <img src="https://img.shields.io/badge/hydra-ED1C24?style=for-the-badge&logo=hydra&logoColor=FFFFFF">
</h3>
<h4 align="center">

**Compiler**
</h4>
<h3 align="center">
  <img src="https://img.shields.io/badge/bazel-3ECF8E?style=for-the-badge&logo=bazel&logoColor=white">
  <img src="https://img.shields.io/badge/llvm-262D3A?style=for-the-badge&logo=llvm&logoColor=white">
  <img src="https://img.shields.io/badge/cmake-064F8C?style=for-the-badge&logo=cmake&logoColor=white">
</h3>
<h4 align="center">

**AI Quantum [Python]**
</h4>
<h3 align="center">
  <img src="https://img.shields.io/badge/qiskit-6929C4?style=for-the-badge&logo=qiskit&logoColor=white">
</h3>
<h4 align="center">

**AI Tools [C++]**
</h4>
<h3 align="center">
  <img src="https://img.shields.io/badge/Mamba-000000?style=for-the-badge&logo=Mamba&logoColor=white">
  <img src="https://img.shields.io/badge/torch-047DA3?style=for-the-badge&logo=torch&logoColor=white">
  <img src="https://img.shields.io/badge/root-0088FF.svg?&style=for-the-badge&logo=root&logoColor=white">
  <img src="https://img.shields.io/badge/cuda-3DCD58?style=for-the-badge&logo=cuda&logoColor=white">
</h3>
<h4 align="center">

**AI Tools [Python]** 
</h4>
<h3 align="center"> 
  <img src="https://img.shields.io/badge/Jupyter-F37626.svg?&style=for-the-badge&logo=Jupyter&logoColor=white">
  <img src="https://img.shields.io/badge/anaconda-44A833?style=for-the-badge&logo=anaconda&logoColor=FFFFFF">
  <img src="https://img.shields.io/badge/Streamlit-FF4B4B?style=for-the-badge&logo=Streamlit&logoColor=white">
  <img src="https://img.shields.io/badge/PyTorch-EE4C2C?style=for-the-badge&logo=pytorch&logoColor=white">
  <img src="https://img.shields.io/badge/TensorFlow-FF6F00?style=for-the-badge&logo=tensorflow&logoColor=white">
  <img src="https://img.shields.io/badge/scikit_learn-F7931E?style=for-the-badge&logo=scikit-learn&logoColor=white">
  <img src="https://img.shields.io/badge/plotly-3F4F75?style=for-the-badge&logo=plotly&logoColor=white">
  <img src="https://img.shields.io/badge/spacy-09A3D5?style=for-the-badge&logo=spacy&logoColor=white">
</h3>
<h3 align="center">

💡 **My stats**
</h3>
<h3 align="center">
<img align="center" alt="kaggle" src="https://road-to-kaggle-grandmaster.vercel.app/api/simple/fuhrerhlemon" width="150" height="25"/>
</h3>
<h3 align="center">
  <img align="center" alt="kaggle" src="https://road-to-kaggle-grandmaster.vercel.app/api/badges/fuhrerhlemon/competition/light" width="80" height="100"/>
  <img align="center" alt="kaggle" src="https://road-to-kaggle-grandmaster.vercel.app/api/badges/fuhrerhlemon/dataset/light" width="80" height="100"/>
  <img align="center" alt="kaggle" src="https://road-to-kaggle-grandmaster.vercel.app/api/badges/fuhrerhlemon/notebook/light" width="80" height="100"/>
</h3>
<div>
  <div align="center">
      <a href="#"><img alt="Fuhrerh-Lemon's Github Stats" src="https://github-readme-stats.vercel.app/api?username=Fuhrerh-Lemon&1nC0re&show_icons=true&include_all_commits=true&count_private=true&theme=react&hide_border=true&bg_color=0D1117&title_color=5ce1e6&icon_color=5ce1e6" height="150"/></a>
      <a href="#"><img alt="Fuhrerh-Lemon's Top Languages" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Fuhrerh-Lemon&1nC0re&langs_count=10&layout=compact&theme=react&hide_border=true&bg_color=0D1117&title_color=5ce1e6&icon_color=5ce1e6" height="150"/></a>
   </div>
 </div>
<div align="center">

![github contribution grid snake animation](https://raw.githubusercontent.com/Fuhrerh-Lemon/Fuhrerh-Lemon/output/github-contribution-grid-snake-dark.svg#gh-dark-mode-only)
</div>
